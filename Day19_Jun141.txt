Handson

1. Create a Java Class com.cts.PerformTask. Containing a method doTask(), which returns a String “hello world”. Call this function from result.JSP page using scriplet. Full package name should not be used inside the scriplet while calling the method. Do this using import attribute of page directive.

2. Car selling application has the logo and the organization’s name in the top of the page. It is common for all the JSPs used in the application. Provide a suitable solution for it.

3. Create a Java Bean Class com.pack.Employee with int code, String name and float ‘salary’ as accessor methods. Create index.JSP containg three text boxes labeled as ‘Name’, ‘Employee Code’, and ‘Salary’. Index.JSP should also contain a submit button. 
 
On submitting the inputs through the text areas of index.JSP the control should pass to another JSP file ‘result.JSP’. Result.JSP file should display the values entered in the previous page as ‘Name’, ‘Employee Code’ and ‘Salary’.
 
Tips:
Use <JSP:useBean>,<JSP:getProperty>, and < JSP:setProperty >


HTML - used to create static webpages

JSP(Java Server Pages)
     - used to create dynamic HTML pages, made up of Java(write business logic) + HTML(design the webpage)
     - inside webapp with .jsp extension
     - No need to compile jsp prg, we can directly execute using http://localhost:8086/J2EEProject/example.jsp

Lifecycle of JSP - 3 stages 
1. Creation - create jsp prg
2. Deployment - it should deployed inside webapp folder
3. Page Translation and compilation - happens when we execute JSP prg - 7 stages
  1. Page Translation - used to check the syntax of JSP 
  2. JSP Page compilation - since JSP is similar to servlet, jsp prg will be converted into HttpServlet prg under prgname_jsp.java
  3. Create an instance for generated servlet class
  4. Load the class 
  5. call jspInit() of javax.servlet.jsp.JspPage interface, it is invoked only once and first when jsp page is loaded, it can be overridden
  6. call _jspService() of javax.servlet.jsp.HttpJspPage interface which extends JspPage interface, it will invoked everytime when a new request comes to jsp page2, it cannot be overridden
  7. call jspDestroy() of javax.servlet.jsp.JspPage interface, it is for destroying purpose, it can be overridden

JSP Elements - 3 elements
1. Directive element - used to give info about jsp page to jsp container - 3 types

a. page directive element
        - used to give info about jsp page to jsp container

Syntax: <%@ page attributes %> - there are 13 attributes

1. language - defines extra scripting language used in JSP - default is java
2. extends - used to extend any user defined class in that particular jsp page
3. import - used to import predefined classes in java for that particular jsp
4. session - defines the session for JSP page, by default all JSP page participate in session, since default value is true
5. autoflush - automatically flush the memory once the buffer size is reached
6. buffer - buffer size - default is 8kb
7. info - any arbitary string about the page
8. isThreadSafe - which indicates jsp to accept multiple request or single request, by default all JSP will accept multiple request 
9. errorPage - defines url of the error page, if any error occurs in that jsp page then it will redirected to error page
10. isErrorPage - indicates that page is a error page or not
11. contentType - define content type of  jsp
12. pageEncoding - defined the character encoding of JSP 
13. isELIgnored - to enable Expression Language in JSP

<%@ page language="java" - default
         extends="com.pack.Sample"
         import="java.util.*,java.time.*,java.io.*"
         session="true(default)/false"
         autoflush="true(default)/false"
         buffer="10kb"
         info="Registration page"
         isThreadSafe="true(default)/false"
         errorPage="error.jsp"
         isErrorPage="true/false(default)"
         contentType="text/html" - default
         pageEncoding="ISO-8859-1" - default
         isELIgnored="true/false(default)"
%>

b. include directive element
       - used to include the output of only static html pages in current page 

Syntax: <%@ include attribute %>

<%@ include file="header.html"%>

<%@ include file="a.html"%>

c. taglib directive elt
       - Normally JSP page can contain predefined tags, apart from predefined tags, jsp can also contain userdefined tags, then info abt jsp page which contain userdefined tag is given to jsp container using taglib elt

Syntax: <%@ taglib uri="taglib uri" prefix="prefix of custom tag" %>

2. Scripting element
       - used to write java code inside jsp - 3 types

a. Declaration
       - used to declare and initialize variable and object creation
Syntax: <%! declaration %>

<%! int i1=0; %>
<%! ArrayList<> l=new ArrayList<>(); %>

b. Scriptlet
       - used to write pure java code
Syntax: <% Scriptlet %>

<% for(int i=0;i<5;i++) {
     out.println(i);
}%>
<% Set<> hs=new HashSet<>(); %>

c. Expression
       - used to print output on the browser
Syntax: <%= expression %>

<! int i=10; %>
<%= i %>
<%! ArrayList<> l=new ArrayList<>(); %>
<%= l.size() %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% List<Integer> l=new ArrayList<>(); 
   l.add(4);
   l.add(5);
   l.add(1);
%>
<%= l.size() %> <br/>

<% for(int i=1;i<=6;i++) { %>
<h<%= i %>>Hello world</h<%=i %>>
<%} %>
<br/>
<% for(int j=1;j<5;j++) { %>
   out.println(j);
  <%} %>
  <br/>
  <%! int k1=10; %>
  <%=++k1 %>
  <br/>
  <% int k2=10; %>
  <%= ++k2 %>
  <br/>
  <% x=5; %>
  <% int x=3; %>
  <%! int x=7; %>
  <%= this.x %>  <%= x %>
</body>
</html>

3. Action elements
        - predefined or standard tags in JSP

1. <jsp:useBean>
       - JSP is made up of Java(write business logic) and HTML(design the webpage), but it is always good practise to separate business logic from presentation logic using Java bean prg
       - Bean is a java prg used to change the properties of webpage at runtime with 3 condition
   1. Bean class should be public and implements Serializable
   2. It should contain only no arg constructor (ie) default constructor
   3. should contain getter and setters method depend on properties of webpage 
       - We need to combine ur bean prg with jsp prg using <jsp:useBean> tag

Syntax: <jsp:useBean attributes>
        </jsp:useBean>

1. id = unique id of bean which acts as object for the bean 
2. scope = lifetime of bean object - page(default)/request/session/application
3. class = fully qualified path of bean class 
4. beanName = serialized object of bean
5. type = name of bean class/class that extends bean class 

4 combination
1. id,scope, class
2. id,scope,type
3. id,scope,class,type
3. id,scope,beaname,type


2. <jsp:setProperty> 
        - used to set value to the bean property

Syntax: <jsp:setProperty attributes />

1. name= id of the bean
2. property= name of the property
3. value=value of the property 

3. <jsp:getProperty> 
        - used to get value for the property

Syntax: <jsp:getProperty name="id of bean" property="propertyname"/>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="http://localhost:8086/J2EEProject/third.jsp">
Name: <input type="text" name="name"/><br/>
Age: <input type="text" name="age"/><br/>
<input type="submit" value="Click">
</form>
</body>
</html>

import java.io.Serializable;

public class User implements Serializable{
   private String name;
   private Integer age;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Integer getAge() {
	return age;
}
public void setAge(Integer age) {
	this.age = age;
}
   
}


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="hello" class="com.pack.User">
    <jsp:setProperty name="hello" property="*"/>
</jsp:useBean>

User with name <jsp:getProperty property="name" name="hello"/> has an age
<jsp:getProperty property="age" name="hello"/> <br/>

User with name <%= hello.getName() %> has an age <%= hello.getAge() %>

</body>
</html>

4. <jsp:forward>
       - used to forward the request to another static or dynamic pages but does not return back to calling page 

Syntax:
1. For static page (html)
      <jsp:forward page="address.html"/>
2. For dynamic page (jsp)
      <jsp:forward page="address.jsp">
         <jsp:param name="state" value="Tamilnadu"/>
      </jsp:forward>

5. <jsp:include>
       - used to include the output of both static and dynamic pages and return back to calling page

Syntax:
1. For static page (html)
      <jsp:include page="address.html" flush="true/false"/>
2. For dynamic page (jsp)
      <jsp:include page="address.jsp" flush="true/false">
         <jsp:param name="state" value="Tamilnadu"/>
      </jsp:include> 












