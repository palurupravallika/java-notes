Handson

emp (eno(pk), ename, bdate, title, salary, dno)
proj (pno(pk), pname, budget, dno)
dept (dno(pk), dname, mgreno)
workson (eno(fk), pno(fk), resp, hours)

Questions:
1) Write an SQL query that returns the project number and name for projects with a budget greater than $100,000.
2) Write an SQL query that returns all works on records where hours worked is less than 10 and the responsibility is 'Manager'.
3) Write an SQL query that returns the employees (number and name only) who have a title of 'EE' or 'SA' and make more than $35,000.
4) Write an SQL query that returns the employees (name only) in department 'D1' ordered by decreasing salary.
5) Write an SQL query that returns the departments (all fields) ordered by ascending department name.
6) Write an SQL query that returns the employee name, department name, and employee title.
7) Write an SQL query that returns the project name, hours worked, and project number for all works on records where hours > 10.
8) Write an SQL query that returns the project name, department name, and budget for all projects with a budget < $50,000.
9) Write an SQL query that returns the employee numbers and salaries of all employees in the 'Consulting' department ordered by descending salary.
10) Write an SQL query that returns the employee name, project name, employee title, and hours for all works on records.



group by clause
    - used to group values in the table based on some aggregate function

Employee table
empid     ename     dept       salary
1           A        HR         4000
2           B        Sales      5000
3           C        HR         6000
4           D        Admin      10000
5           E        Sales      5000
6           F        Admin      8000
7           G        HR         10000

We want to calculate the total sum of salary 
>select sum(salary) from employee;

We want to calculate the total sum of salary in each department
>select sum(salary) as 'TotalSalary', dept from employee group by dept;

TotalSalary     dept
20000           HR
10000           Sales
18000           Admin

having clause
    - used to exclude the result from group by clause

>select sum(salary) as 'TotalSalary', dept from employee group by dept having sum(salary)>15000;

TotalSalary     dept
20000           HR
18000           Admin

Subqueries
     - select query inside another select query
     - used to retrieve data based on some unknown condition
     - Inner query will be executed first, based on the output outer query will be executed 
     - 2 types
1. Single row subquery - inner query returns a single row, then it is evaluated with outer query using =,!=,>,<,>=,<=
2. Multi row subquery - inner query returns a multiple row, then it is evaluated with outer query using in,not in,any,all,exists,not exists 


mysql> create table emp(eid int primary key,efirstname varchar(20),elastname varchar(20),ecity char(15));
Query OK, 0 rows affected (0.07 sec)

mysql> insert into emp values(1,'Ram','Kumar','Chennai');
Query OK, 1 row affected (0.01 sec)

mysql> insert into emp values(2,'Sam','Kumar','Mumbai');
Query OK, 1 row affected (0.01 sec)

mysql> insert into emp values(3,'Saj','Raj','Delhi');
Query OK, 1 row affected (0.01 sec)

mysql> insert into emp values(4,'Tim','Mike','Pune');
Query OK, 1 row affected (0.01 sec)

mysql> insert into emp values(5,'Tom','Jerry','Bangalore');
Query OK, 1 row affected (0.01 sec)

mysql> insert into emp values(6,'John','Smith','Hyderabad');
Query OK, 1 row affected (0.01 sec)

mysql> create table dept(did int primary key,dname varchar(20),empid int references emp(eid),dsal float);
Query OK, 0 rows affected (0.05 sec)

mysql> insert into dept values(1001,'HR',1,5000);
Query OK, 1 row affected (0.01 sec)

mysql> insert into dept values(1002,'Sales',2,10000);
Query OK, 1 row affected (0.01 sec)

mysql> insert into dept values(1003,'HR',3,5000);
Query OK, 1 row affected (0.01 sec)

mysql> insert into dept values(1004,'Sales',4,10000);
Query OK, 1 row affected (0.01 sec)

mysql> insert into dept values(1005,'Admin',5,12000);
Query OK, 1 row affected (0.01 sec)

mysql> insert into dept values(1006,'Admin',6,15000);
Query OK, 1 row affected (0.01 sec)

mysql> select * from emp;
+-----+------------+-----------+-----------+
| eid | efirstname | elastname | ecity     |
+-----+------------+-----------+-----------+
|   1 | Ram        | Kumar     | Chennai   |
|   2 | Sam        | Kumar     | Mumbai    |
|   3 | Saj        | Raj       | Delhi     |
|   4 | Tim        | Mike      | Pune      |
|   5 | Tom        | Jerry     | Bangalore |
|   6 | John       | Smith     | Hyderabad |
+-----+------------+-----------+-----------+
6 rows in set (0.01 sec)

mysql> select * from dept;
+------+-------+-------+-------+
| did  | dname | empid | dsal  |
+------+-------+-------+-------+
| 1001 | HR    |     1 |  5000 |
| 1002 | Sales |     2 | 10000 |
| 1003 | HR    |     3 |  5000 |
| 1004 | Sales |     4 | 10000 |
| 1005 | Admin |     5 | 12000 |
| 1006 | Admin |     6 | 15000 |
+------+-------+-------+-------+
6 rows in set (0.01 sec)

mysql> select * from emp where eid=(select empid from dept where dsal=12000);
+-----+------------+-----------+-----------+
| eid | efirstname | elastname | ecity     |
+-----+------------+-----------+-----------+
|   5 | Tom        | Jerry     | Bangalore |
+-----+------------+-----------+-----------+
1 row in set (0.01 sec)

mysql> select * from emp where eid in(select empid from dept where dsal<10000);
+-----+------------+-----------+---------+
| eid | efirstname | elastname | ecity   |
+-----+------------+-----------+---------+
|   1 | Ram        | Kumar     | Chennai |
|   3 | Saj        | Raj       | Delhi   |
+-----+------------+-----------+---------+
2 rows in set (0.01 sec)

mysql> select * from emp where eid =(select empid from dept where dsal<10000);
ERROR 1242 (21000): Subquery returns more than 1 row
mysql> select * from emp where eid > any(select empid from dept where dsal<10000);
+-----+------------+-----------+-----------+
| eid | efirstname | elastname | ecity     |
+-----+------------+-----------+-----------+
|   2 | Sam        | Kumar     | Mumbai    |
|   3 | Saj        | Raj       | Delhi     |
|   4 | Tim        | Mike      | Pune      |
|   5 | Tom        | Jerry     | Bangalore |
|   6 | John       | Smith     | Hyderabad |
+-----+------------+-----------+-----------+
5 rows in set (0.00 sec)

mysql> select * from emp where eid > all(select empid from dept where dsal<10000);
+-----+------------+-----------+-----------+
| eid | efirstname | elastname | ecity     |
+-----+------------+-----------+-----------+
|   4 | Tim        | Mike      | Pune      |
|   5 | Tom        | Jerry     | Bangalore |
|   6 | John       | Smith     | Hyderabad |
+-----+------------+-----------+-----------+
3 rows in set (0.00 sec)

mysql> select * from emp where eid = all(select empid from dept where dsal>12000);
+-----+------------+-----------+-----------+
| eid | efirstname | elastname | ecity     |
+-----+------------+-----------+-----------+
|   6 | John       | Smith     | Hyderabad |
+-----+------------+-----------+-----------+
1 row in set (0.00 sec)

mysql> select * from emp where exists(select * from dept where dsal=16000);
Empty set (0.00 sec)

mysql> select * from emp where exists(select * from dept where dsal=15000);
+-----+------------+-----------+-----------+
| eid | efirstname | elastname | ecity     |
+-----+------------+-----------+-----------+
|   1 | Ram        | Kumar     | Chennai   |
|   2 | Sam        | Kumar     | Mumbai    |
|   3 | Saj        | Raj       | Delhi     |
|   4 | Tim        | Mike      | Pune      |
|   5 | Tom        | Jerry     | Bangalore |
|   6 | John       | Smith     | Hyderabad |
+-----+------------+-----------+-----------+
6 rows in set (0.00 sec)

mysql> select * from emp where not exists(select * from dept where dsal=15000);
Empty set (0.00 sec)

mysql> select * from emp where not exists(select * from dept where dsal=16000);
+-----+------------+-----------+-----------+
| eid | efirstname | elastname | ecity     |
+-----+------------+-----------+-----------+
|   1 | Ram        | Kumar     | Chennai   |
|   2 | Sam        | Kumar     | Mumbai    |
|   3 | Saj        | Raj       | Delhi     |
|   4 | Tim        | Mike      | Pune      |
|   5 | Tom        | Jerry     | Bangalore |
|   6 | John       | Smith     | Hyderabad |
+-----+------------+-----------+-----------+
6 rows in set (0.00 sec)

Joins
   - at the time of displaying we can join the table and get the data 

Types
1. Equi join
        - we join the table based on some equality conditions

mysql> create table dept1(deptid int primary key,dname varchar(15));
Query OK, 0 rows affected (0.05 sec)

mysql> insert into dept1 values(10,'Sales');
Query OK, 1 row affected (0.01 sec)

mysql> insert into dept1 values(20,'Admin');
Query OK, 1 row affected (0.01 sec)

mysql> insert into dept1 values(30,'HR');
Query OK, 1 row affected (0.01 sec)

mysql> insert into dept1 values(40,'Operations');
Query OK, 1 row affected (0.01 sec)

mysql> create table emp1(empid int primary key, ename varchar(20),deptid int references dept(deptid));
Query OK, 0 rows affected (0.23 sec)

mysql> insert into emp1 values(1,'Pat',10);
Query OK, 1 row affected (0.01 sec)

mysql> insert into emp1 values(2,'Sat',20);
Query OK, 1 row affected (0.01 sec)

mysql> insert into emp1 values(3,'Pack',10);
Query OK, 1 row affected (0.01 sec)

mysql> insert into emp1(empid,ename) values(4,'Mack');
Query OK, 1 row affected (0.01 sec)

mysql> select * from emp1;
+-------+-------+--------+
| empid | ename | deptid |
+-------+-------+--------+
|     1 | Pat   |     10 |
|     2 | Sat   |     20 |
|     3 | Pack  |     10 |
|     4 | Mack  |   NULL |
+-------+-------+--------+
4 rows in set (0.00 sec)

mysql> select * from dept1;
+--------+------------+
| deptid | dname      |
+--------+------------+
|     10 | Sales      |
|     20 | Admin      |
|     30 | HR         |
|     40 | Operations |
+--------+------------+
4 rows in set (0.00 sec)

mysql> select e.ename,d.dname from emp1 e,dept1 d where e.deptid=d.deptid;
+-------+-------+
| ename | dname |
+-------+-------+
| Pat   | Sales |
| Sat   | Admin |
| Pack  | Sales |
+-------+-------+
3 rows in set (0.00 sec)

2. Inner join
        - similar to equi join

mysql> select e.ename,d.dname from emp1 e inner join dept1 d on e.deptid=d.deptid;
+-------+-------+
| ename | dname |
+-------+-------+
| Pat   | Sales |
| Sat   | Admin |
| Pack  | Sales |
+-------+-------+
3 rows in set (0.00 sec)

3. Natural join
         - creates an implicit join clause based on common columns in the tables

mysql> select * from emp1 natural join dept1;
+--------+-------+-------+-------+
| deptid | empid | ename | dname |
+--------+-------+-------+-------+
|     10 |     1 | Pat   | Sales |
|     20 |     2 | Sat   | Admin |
|     10 |     3 | Pack  | Sales |
+--------+-------+-------+-------+
3 rows in set (0.00 sec)

4. Outer join
       - To display both matched and unmatched rows 

3 types
1. Left outer join
       - all rows from left table + only matched rows from right table

mysql> select e.ename,d.dname from emp1 e left join dept1 d on e.deptid=d.deptid;
+-------+-------+
| ename | dname |
+-------+-------+
| Pat   | Sales |
| Sat   | Admin |
| Pack  | Sales |
| Mack  | NULL  |
+-------+-------+
4 rows in set (0.00 sec)

2. Right outer join
        - all rows from right table + only matched rows from left table 

mysql> select e.ename,d.dname from emp1 e right join dept1 d on e.deptid=d.deptid;
+-------+------------+
| ename | dname      |
+-------+------------+
| Pat   | Sales      |
| Sat   | Admin      |
| Pack  | Sales      |
| NULL  | HR         |
| NULL  | Operations |
+-------+------------+
5 rows in set (0.00 sec)

3. Full outer join
      - all matched and unmatched rows from both the table
      - In mysql there is no full outer join, then we have to take union of left outer join and right outer join

mysql> select e.ename,d.dname from emp1 e left join dept1 d on e.deptid=d.deptid
    ->       union
    -> select e.ename,d.dname from emp1 e right join dept1 d on e.deptid=d.deptid;
+-------+------------+
| ename | dname      |
+-------+------------+
| Pat   | Sales      |
| Sat   | Admin      |
| Pack  | Sales      |
| Mack  | NULL       |
| NULL  | HR         |
| NULL  | Operations |
+-------+------------+
6 rows in set (0.00 sec)

5. Cross join
       - cartesian product of two table

>select * from emp1,dept1;
       or
>select * from emp1 cross join dept1;

4. TCL - Transaction Control Language
         - Transaction is a small unit of work performed against the database (ie) while inserting, updating, deleting

1. commit - used to commit the transaction performed on db
2. rollback - undo the all transaction before commiting, only for DML
commit;
savepoint A;
insert
insert;
Savepoint B;
update;
Savepoint C;
delete;
insert;
rollback to B;
3. savepoint - saving the transaction

insert;
delete;
create; - when we call DDL, then autocommit will takes place
delete
rollback;


mysql> select * from emp1;
+-------+-------+--------+
| empid | ename | deptid |
+-------+-------+--------+
|     1 | Pat   |     10 |
|     2 | Sat   |     20 |
|     3 | Pack  |     10 |
|     4 | Mack  |   NULL |
+-------+-------+--------+
4 rows in set (0.00 sec)

mysql> START TRANSACTION;
Query OK, 0 rows affected (0.00 sec)

mysql> SAVEPOINT initial;
Query OK, 0 rows affected (0.00 sec)

mysql> insert into emp1 values(5,'Taj',30);
Query OK, 1 row affected (0.00 sec)

mysql> SAVEPOINT addition;
Query OK, 0 rows affected (0.00 sec)

mysql> update emp1 set name='Tajjj' where empid=5;
ERROR 1054 (42S22): Unknown column 'name' in 'field list'
mysql> update emp1 set ename='Tajjj' where empid=5;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> update emp1 set ename='Tajjj' where empid=5;
Query OK, 0 rows affected (0.00 sec)
Rows matched: 1  Changed: 0  Warnings: 0

mysql> select * from emp1;
+-------+-------+--------+
| empid | ename | deptid |
+-------+-------+--------+
|     1 | Pat   |     10 |
|     2 | Sat   |     20 |
|     3 | Pack  |     10 |
|     4 | Mack  |   NULL |
|     5 | Tajjj |     30 |
+-------+-------+--------+
5 rows in set (0.00 sec)

mysql> SAVEPOINT updation;
Query OK, 0 rows affected (0.00 sec)

mysql> delete from emp1 where empid=5;
Query OK, 1 row affected (0.00 sec)

mysql> select * from emp1;
+-------+-------+--------+
| empid | ename | deptid |
+-------+-------+--------+
|     1 | Pat   |     10 |
|     2 | Sat   |     20 |
|     3 | Pack  |     10 |
|     4 | Mack  |   NULL |
+-------+-------+--------+
4 rows in set (0.00 sec)

mysql> SAVEPOINT deletion;
Query OK, 0 rows affected (0.00 sec)

mysql> rollback to updation;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from emp1;
+-------+-------+--------+
| empid | ename | deptid |
+-------+-------+--------+
|     1 | Pat   |     10 |
|     2 | Sat   |     20 |
|     3 | Pack  |     10 |
|     4 | Mack  |   NULL |
|     5 | Tajjj |     30 |
+-------+-------+--------+
5 rows in set (0.00 sec)

mysql> rollback to addition;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from emp1;
+-------+-------+--------+
| empid | ename | deptid |
+-------+-------+--------+
|     1 | Pat   |     10 |
|     2 | Sat   |     20 |
|     3 | Pack  |     10 |
|     4 | Mack  |   NULL |
|     5 | Taj   |     30 |
+-------+-------+--------+
5 rows in set (0.00 sec)

mysql> commit;
Query OK, 0 rows affected (0.00 sec)

5. DCL - Data Control Language
       - GRANT - grant privilege to user 
       - REVOKE - remove privilege from user

>GRANT select,update on employee to ram;
>REVOKE select,update on employee to ram;

Schema - anything that is stored inside db (eg) table, views, procedure etc

Views
    - It is also a schema object 
    - It is called as virtual table because it dosent have its own data, it always contain data of another table and that table is called as base table
    - used to execute complex queries 

2 types
1. Simple view
2. Complex view

Syntax: create or [replace] view viewname as select stmt;

mysql> select e.ename,d.dname from emp1 e,dept1 d where e.deptid=d.deptid;
+-------+-------+
| ename | dname |
+-------+-------+
| Pat   | Sales |
| Sat   | Admin |
| Pack  | Sales |
| Taj   | HR    |
+-------+-------+
4 rows in set (0.00 sec)

mysql> create view v1 as  select e.ename,d.dname from emp1 e,dept1 d where e.deptid=d.deptid;
Query OK, 0 rows affected (0.02 sec)

mysql> select * from v1;
+-------+-------+
| ename | dname |
+-------+-------+
| Pat   | Sales |
| Sat   | Admin |
| Pack  | Sales |
| Taj   | HR    |
+-------+-------+
4 rows in set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_batch2 |
+------------------+
| cust7            |
| customer3        |
| customer4        |
| customer5        |
| customer6        |
| department       |
| dept             |
| dept1            |
| emp              |
| emp1             |
| employee         |
| v1               |
+------------------+
12 rows in set (0.01 sec)

mysql> show full tables;
+------------------+------------+
| Tables_in_batch2 | Table_type |
+------------------+------------+
| cust7            | BASE TABLE |
| customer3        | BASE TABLE |
| customer4        | BASE TABLE |
| customer5        | BASE TABLE |
| customer6        | BASE TABLE |
| department       | BASE TABLE |
| dept             | BASE TABLE |
| dept1            | BASE TABLE |
| emp              | BASE TABLE |
| emp1             | BASE TABLE |
| employee         | BASE TABLE |
| v1               | VIEW       |
+------------------+------------+
12 rows in set (0.00 sec)

mysql> drop table emp1;
Query OK, 0 rows affected (0.04 sec)

mysql> show full tables;
+------------------+------------+
| Tables_in_batch2 | Table_type |
+------------------+------------+
| cust7            | BASE TABLE |
| customer3        | BASE TABLE |
| customer4        | BASE TABLE |
| customer5        | BASE TABLE |
| customer6        | BASE TABLE |
| department       | BASE TABLE |
| dept             | BASE TABLE |
| dept1            | BASE TABLE |
| emp              | BASE TABLE |
| employee         | BASE TABLE |
| v1               | VIEW       |
+------------------+------------+
11 rows in set (0.00 sec)

mysql> select * from v1;
ERROR 1356 (HY000): View 'batch2.v1' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them

Updatable views
     - Certain views are updatable (ie) if we perform any DML operation on view it will affect the base table but with following conditions on select stmt
  1. select stmt should not contain aggregate function, distinct, union,union all operator
  2. group by, having clause
  3. left join, outer join

mysql> create view v2 as select * from dept1;
Query OK, 0 rows affected (0.01 sec)

mysql> desc v2;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| deptid | int         | NO   |     | NULL    |       |
| dname  | varchar(15) | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

mysql> select * from v2;
+--------+------------+
| deptid | dname      |
+--------+------------+
|     10 | Sales      |
|     20 | Admin      |
|     30 | HR         |
|     40 | Operations |
+--------+------------+
4 rows in set (0.00 sec)

mysql> update v2 set dname='Finance' where deptid=40;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from v2;
+--------+---------+
| deptid | dname   |
+--------+---------+
|     10 | Sales   |
|     20 | Admin   |
|     30 | HR      |
|     40 | Finance |
+--------+---------+
4 rows in set (0.00 sec)

mysql> select * from dept1;
+--------+---------+
| deptid | dname   |
+--------+---------+
|     10 | Sales   |
|     20 | Admin   |
|     30 | HR      |
|     40 | Finance |
+--------+---------+
4 rows in set (0.00 sec)

mysql> insert into v2 values(50,'Accounts');
Query OK, 1 row affected (0.01 sec)

mysql> select * from v2;
+--------+----------+
| deptid | dname    |
+--------+----------+
|     10 | Sales    |
|     20 | Admin    |
|     30 | HR       |
|     40 | Finance  |
|     50 | Accounts |
+--------+----------+
5 rows in set (0.00 sec)

mysql> select * from dept1;
+--------+----------+
| deptid | dname    |
+--------+----------+
|     10 | Sales    |
|     20 | Admin    |
|     30 | HR       |
|     40 | Finance  |
|     50 | Accounts |
+--------+----------+
5 rows in set (0.00 sec)

with check option in views
     - Certain views are updatable, but at the time of updating the view if we want to check some constraint

mysql> create view v3 as select * from dept1 where deptid>40 with check option;
Query OK, 0 rows affected (0.01 sec)

mysql> select * from v3;
+--------+----------+
| deptid | dname    |
+--------+----------+
|     50 | Accounts |
+--------+----------+
1 row in set (0.00 sec)

mysql> insert into v3 values(60,'Operations');
Query OK, 1 row affected (0.01 sec)

mysql> select * from v3;
+--------+------------+
| deptid | dname      |
+--------+------------+
|     50 | Accounts   |
|     60 | Operations |
+--------+------------+
2 rows in set (0.00 sec)

mysql> insert into v3 values(35,'Order');
ERROR 1369 (HY000): CHECK OPTION failed 'batch2.v3'

//renaming view
mysql> rename table v3 to v4;
Query OK, 0 rows affected (0.01 sec)

//deleting view
mysql> drop view v4;
Query OK, 0 rows affected (0.01 sec)


mysql> create view v4 as select * from dept1;
Query OK, 0 rows affected (0.01 sec)

//Altering the view 
mysql> create or replace view v4 as select * from dept1 where deptid>20;
Query OK, 0 rows affected (0.01 sec)

Index
   - used for faster retrieval of data if ur table contains huge number of data, internally it will create rowid
   - Index will be automatically created for primary key and unique key 

Syntax:  create index indexname on tablename(column1,column2....);

mysql> select * from dept1;
+--------+------------+
| deptid | dname      |
+--------+------------+
|     10 | Sales      |
|     20 | Admin      |
|     30 | HR         |
|     40 | Finance    |
|     50 | Accounts   |
|     60 | Operations |
+--------+------------+
6 rows in set (0.00 sec)

mysql> create index idx1 on dept1(dname);
Query OK, 0 rows affected (0.09 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> show indexes from dept1;
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| Table | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment | Visible | Expression |
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| dept1 |          0 | PRIMARY  |            1 | deptid      | A         |           6 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
| dept1 |          1 | idx1     |            1 | dname       | A         |           6 |     NULL |   NULL | YES  | BTREE      |         |               | YES     | NULL       |
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
2 rows in set (0.03 sec)

mysql> drop index idx1 on dept1;
Query OK, 0 rows affected (0.04 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> show indexes from dept1;
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| Table | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment | Visible | Expression |
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| dept1 |          0 | PRIMARY  |            1 | deptid      | A         |           6 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
+-------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
1 row in set (0.00 sec)






























