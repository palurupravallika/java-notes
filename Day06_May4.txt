Object class
1. String toString()
2. boolean equals(Object o)
3. ==
4. int hashCode()
5. final void wait()
6. final void wait(long msec)
7. final void notify()
8. final void notifyAll()
9. protected void finalize()
       - used to perform cleanup activity before destroying the object 
       - called by default for every object before its deletion and only once in lifecycle of the program

Different ways
1. By anonymous object - are thos object which are created without any reference variable
      new Sample();

2. By nulling reference
       Sample s=new Sample();
       s=null; 

3. By assigning reference to another object 
     Student s1=new Student();  //1000
     Student s2=new Student();  //1001
     s1=s2;

To explicitly call garbage collector using System.gc(), that invokes garbage collector which destroy unused object, that internally call finalize() only once for each object

public class Main {
	
	public static void main(String[] args) {
		Main m=new Main();
		m=null;
		System.gc();
		
	}
	protected void finalize() {
		System.out.println("Finalize called");
	}

}


public class Main {
	
	public static void main(String[] args) {
		String s1=new String("Hello");
		s1=null;
		System.gc();
		System.out.println("Hello world");
		
	}
	protected void finalize() {
		System.out.println("Finalize called");
	}

}

public class Main {
	
	public static void main(String[] args) {
		Main s1=new Main();
		s1=null;
		System.gc();
		System.out.println("Hello world");
		
	}
	protected void finalize() {
		System.out.println("Finalize called");
	}

}

finalize() is called only once for a object
public class Main {
	
	public static void main(String[] args) {
		Main m1=new Main();
		m1=null;
		System.gc();
		System.gc();
		System.out.println("Hello world");
		
	}
	protected void finalize() {
		System.out.println("Finalize called");
	}

}

public class Main {
	
	public static void main(String[] args) {
		Main m1=new Main();
		Main m2=new Main();
		m1=m2;
		m1.finalize();
		System.gc();
		System.out.println("Hello world");
		
	}
	protected void finalize() {
		System.out.println("Finalize called");
	}

}

10. Object clone()
         - used to create the exact copy of the object 
         - clone() always follow shallow copy
         - If we want to take copy of object using clone() then that class should implement an interface called Cloneable which is a marker interface, in case if it is not implemented Cloneable interface it will throw CloneNotSupportedException

Shallow copy 
     - will have exact copy of all fields of original object (ie) any changes made to thos objects through clone object will be reflected in original object and vice versa

class Address {
	String street;
	String country;
	String city;
	public Address(String street, String country, String city) {
		super();
		this.street = street;
		this.country = country;
		this.city = city;
	}
}
class Employee implements Cloneable{
	int id;
	String name;
    Address address;
	public Employee(int id, String name, Address address) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
	}
    
	protected Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
}
public class Main {
	
	public static void main(String[] args) {
		Address addr=new Address("xyz","India","ABC");
		Employee e1=new Employee(100,"Ram",addr);
		Employee e2=null;
		try {
			e2=(Employee)e1.clone();
		}
		catch(CloneNotSupportedException e) {
			e.printStackTrace();
		}
		System.out.println(e1.address.country); //India
		e2.address.country="Russia";
		System.out.println(e1.address.country); //Russia
	}
	

}


Deep copy
     - will have exact copy of all fields of original object, and any changes made to clone object will not be reflected in original object and vice versa

class Address implements Cloneable  {
	String street;
	String country;
	String city;
	public Address(String street, String country, String city) {
		super();
		this.street = street;
		this.country = country;
		this.city = city;
	}
	protected Object clone() throws CloneNotSupportedException{
		return super.clone();
	}
}
class Employee implements Cloneable{
	int id;
	String name;
    Address address;
	public Employee(int id, String name, Address address) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
	}
    
	protected Object clone() throws CloneNotSupportedException{
		Employee e=(Employee)super.clone();
		e.address=(Address)address.clone();
		return e;
	}
}
public class Main {
	
	public static void main(String[] args) {
		Address addr=new Address("xyz","India","ABC");
		Employee e1=new Employee(100,"Ram",addr);
		Employee e2=null;
		try {
			e2=(Employee)e1.clone();
		}
		catch(CloneNotSupportedException e) {
			e.printStackTrace();
		}
		System.out.println(e1.address.country); //India
		e2.address.country="Russia";
		System.out.println(e1.address.country); //India 
	}
	

}


2. Wrapper class 
       - classes that supports the primitive datatype, to perform any operation on the datatype
       - All wrapper classes are immutable(u cant change)

int a=10;

Datatype    Wrapper class 
int        - Integer
float      - Float
double     - Double
short      - Short
byte       - Byte
long       - Long
boolean    - Boolean
char       - Character

1. Float class
       - used to perform operation on float datatype 

Syntax: public class Float extends Number implements Comparable 

Constructor
1. Float(double)
      Float f1=new Float(3.14);
2. Float(float)
      Float f2=new Float(3.14f);
3. Float(String) throws NumberFormatException
      Float f3=new Float("3.14");
      Float f4=new Float("abc"); //throws NFE

Methods

int a=5, b=5;  a==b
float a1=3.14, b1=3.14; 

1. static int compare(float f1,float f2) - compare two float datatype, if equal it returns 0, if greater 1, if lesser means -1
2. int compareTo(Float f) - compare 2 Float object, if equal it returns 0, if greater 1, if lesser means -1
3. static boolean isInfinite() - check float datatype to be infinite or not
4. boolean isInfinite() - check Float object 
5. static boolean isFinite() - check float datatypes to be finite or not
6. boolean isFinite() - check Float object
7. static boolean isNaN() - check float datatype as not a number
8. boolean isNaN() - check Float object 
9. static float parseFloat(String s) throws NumberFormatException - used convert String to float datatype
10. static Float valueOf(String s) throws NFE - used to convert String to Float wrapper class
11. float floatValue() - used to convert Float wrapper class to float datatype
12. int intValue()
13. double doubleValue()
14. long longValue()
15. short shortValue()
16. byte byteValue()

Constants
1. Float.MAX_VALUE
2. Float.MIN_VALUE
3. Float.POSITIVE_INFINITY
4. Float.NEGATIVE_INFINITY
5. Float.TYPE

public class Main {
	
	public static void main(String[] args) {
		float f1=3.14f;
		float f2=3.14f;
		System.out.println(Float.compare(f1, f2)); //0
		
		Float f3=new Float(3.14f);
		Float f4=new Float(3.14f);
		System.out.println(f3.compareTo(f4)); //0
		
		float f5=(float)(1/0.);
		System.out.println(Float.isInfinite(f5)); //true
		Float f6=new Float(1/0.);
		System.out.println(f6.isInfinite()); //true
		
		float f7=(float)Math.sqrt(-4);
		System.out.println(Float.isNaN(f7)); //true
		Float f8=new Float(Math.sqrt(-4));
		System.out.println(f8.isNaN()); //true
		
		String s="3.14";
		float f9=Float.parseFloat(s);
		System.out.println(f9); //3.14
		
		Float f10=Float.valueOf(s);
		System.out.println(f10); //3.14
		float f11=f10.floatValue();
		System.out.println(f11); //3.14
		int i=f10.intValue();
		System.out.println(i); //3
		
		System.out.println(Float.MAX_VALUE);
		System.out.println(Float.MIN_VALUE);
		System.out.println(Float.POSITIVE_INFINITY);
		System.out.println(Float.NEGATIVE_INFINITY);
		System.out.println(Float.TYPE);
	}
	

}

2. Double class
        - used to perform operation on double datatype

Constructor
   1. Double(double)
   2. Double(String) throws NFE 

3. Integer, Short, Byte, Long wrapper class
       - used to perform operation on int, short, byte, long datatype respectively 

Constructor
1. Integer(int i)
     - Integer i=new Integer(30);
   Integer(String s) throws NFE
     - Integer i=new Integer("30");

2. Short(short s)
      - Short s1=new Short(23);  //error
      - Short s1=new Short((short)23); //correct
   Short(String s) throws NFE
      - Short s2=new Short("23");

3. Byte(byte b)
      - Byte b1=new Byte(20);  //error
      - Byte b2=new Byte((byte)20); //correct
   Byte(String s) throws NFE
      - Byte b3=new Byte("20");

4. Long(long g)
     - Long l1=new Long(10);
     - Long l2=new Long(10l);
   Long(String s) throws NFE
     - Long l3=new Long("10");

Methods
1. static int parseInt(String d)
2. static int parseInt(String s, int radix)     
  
public class Main {
	
	public static void main(String[] args) {
		int i1=Integer.parseInt("42");
		System.out.println(i1); //42
		
		int i2=Integer.parseInt("42", 5);
		//2*5^0=2
		//4*5^1=20
		System.out.println(i2); //22
		System.out.println(Integer.toBinaryString(2)); //10
		System.out.println(Integer.toOctalString(9)); //11
		System.out.println(Integer.toHexString(10)); //a
		
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.MIN_VALUE);
		System.out.println(Integer.TYPE); //int
	}
	

}

3. Boolean class
      - used to perform operation on boolean datatype

Constructor
1. Boolean(boolean b)
Boolean b1=new Boolean(true); 
System.out.println(b1); //true
Boolean b2=new Boolean(false); 
System.out.println(b2); //false
Boolean b3=new Boolean(True);  //error
System.out.println(b3); 
Boolean b4=new Boolean(hello); //error
System.out.println(b4);
2. Boolean(String s) - we can give only true in any format it will return true, other than true if we give anything it return false

Boolean b1=new Boolean("true"); 
System.out.println(b1); //true
Boolean b2=new Boolean("True"); 
System.out.println(b2); //true
Boolean b3=new Boolean("false"); 
System.out.println(b3); //false
Boolean b4=new Boolean("hello"); 
System.out.println(b4); //false
Boolean b5=new Boolean("TRUE"); 
System.out.println(b5); //true

4. Character class 
     - used to perform operation on char datatype

Constructor
1. Character(char c)

Methods
1. static boolean isDigit(char c)
2. static boolean isLetter(char c)
3. static boolean isLetterOrDigit(char c)
4. static boolean isLowerCase(char c)
5. static boolean isUpperCase(char c)
6. static boolean isSpace(char c)
7. static char toLowerChar(char c)
8. static char toUpperCase(char c)

Autoboxing and Unboxing
    - Introduced from JDK1.5
    - Autoboxing is automatic conversion of primitive datatype to wrapper class
    - Unboxing is automatic conversion of wrapper class to datatype

with autoboxing                           without autoboxing
Integer i;                                Integer i;
int j;                                    int j;
i=10; //Autoboxing                        i=new Integer(10);
j=i; //unboxing                           j=i.intValue();


Short s;
Boolean b=(s instanceof Short);

Integer i=10;
int j=i;

3. String class 
      - String is final class in java.lang.*
      - Fixed length of character, hence it is immutable class (ie) we cant increase or decrease its size at runtime

Syntax: public final class String extends Object implements Serializable, Comparable, CharSequence

Constructor
1. String()
2. String(String s)
3. String(byte[] b)
4. String(byte[] b,int start, int offset)
5. String(char[] c)
6. String(char[] c,int start,int offset)
7. String(StringBuffer s)

Methods
1. String toString() - String representation of object
2. char charAt(int position) - return a single char
3. void getChars(int start,int end, char buf[], int targetstart) - return group of char
4. byte[] getBytes() - convert string to byte[]
5. char[] toCharArray() - convert string to char[]
6. boolean equals(String d) - check equality of content taking case into consideration
7. boolean equalsIgnoreCase(String d) - check equality of content without taking case into consideration
8. == - check equality of object reference
9. boolean startsWith(String s)
10. boolean endsWith(String s)
11. String toLowerCase()
12. String toUpperCase()
13. int compareTo(String s) - compare 2 string taking case into consideration and if both are equal return o, greater return +ve, lesser return -ve
14. int compareToIgnoreCase(String s) - compare 2 string without taking case into consideration and if both are equal return o, greater return +ve, lesser return -ve

public class Main {
	
	public static void main(String[] args) {
		byte[] b= {65,66,67,68,69};
		String s1=new String(b);
		System.out.println(s1); //ABCDE
		String s2=new String(b,1,3); //BCD
		
		char[] c= {'J','A','V','A'};
		String s3=new String(c);
		System.out.println(s3); //JAVA
		String s4=new String(c,0,2);
		System.out.println(s4); //JA
		String s5=new String(s4);
		System.out.println(s5); //JA
		
		String s6="Hello";  //literal
		System.out.println(s6.charAt(1)); //e
		
		String s7="This is a demo of getChars method";
		int start=10; int end=14;
		char buf[]=new char[end-start]; //4
		s7.getChars(start, end, buf, 0);
		System.out.println(buf); //demo
		
		String s8="ABCD";
		byte[] b1=s8.getBytes();
		for(byte b2:b1)
			System.out.println(b2); //65 66 67 69
		
		String s9="ABCD";
		char c1[]=s9.toCharArray();
		for(char c2:c1)
			System.out.println(c2); //A B C D
		
		String s10=new String("Hello");
		String s11=new String("Hello");
		String s12=new String("hello");
		System.out.println(s10.equals(s11)); //true
		System.out.println(s10.equals(s12)); //false
		System.out.println(s10.equalsIgnoreCase(s12)); //true
		System.out.println(s11==s12); //false
		String s13=s12;
		System.out.println(s12==s13); //true
		
		System.out.println("Foobar".startsWith("Foo")); //true
		System.out.println("Foobar".endsWith("bar")); //true
		System.out.println("Foobar".toLowerCase());
		System.out.println("Foobar".toUpperCase());
		
		System.out.println("hello".compareTo("hello")); //0
		System.out.println("check".compareTo("hello")); //-5
		System.out.println("hello".compareTo("check")); //5
		System.out.println("hell".compareTo("heck")); //9
		
	}

}
